﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trivia
{
    public class Game
    {
        private readonly List<string> _players = new List<string>();

        private readonly Dictionary<string, bool> jokers = new Dictionary<string, bool>();
        public List<Player> players = new List<Player>();
        public Player currentPlayer;
        public int nextCat;
        
        private readonly List<int> _places = new List<int>();
        private readonly List<int> _purses = new List<int>();

        private readonly List<bool> _inPenaltyBox = new List<bool>();

        private readonly LinkedList<string> _popQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _scienceQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _sportsQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _rockQuestions = new LinkedList<string>();
        private readonly LinkedList<string> _technoQuestions = new LinkedList<string>();
        private readonly Dictionary<EQuestionType, LinkedList<string>> questions = new Dictionary<EQuestionType, LinkedList<string>>();

        public List<Player> leaderBoard { get; set; } = new List<Player>();

        private bool _isGettingOutOfPenaltyBox;

        public bool isTechno { get; set; }
        public int nbWinOr { get; set; }
        public Game()
        {
            
            for (var i = 0; i < 50; i++)
            {
                _popQuestions.AddLast("Pop Question " + i);
                _scienceQuestions.AddLast(("Science Question " + i));
                _sportsQuestions.AddLast(("Sports Question " + i));
                _rockQuestions.AddLast(isTechno ? CreateTechnoQuestion(i) : CreateRockQuestion(i));
            }
        }

        public string CreateRockQuestion(int index)
        {
            return "Rock Question " + index;
        }

        public string CreateTechnoQuestion(int index)
        {
            return "Techno Question " + index;
        }

        public bool Add(string playerName)
        {
            this.players.Add(new Player
            {
                Name = playerName,
                Purses = 0,
                HasPenality = false,
                Place = 0,
                Rank = this.HowManyPlayers(),
                HasJoker = EJoker.HAS,
                ScoringLevel = 1,
                TimeinPrison = 0,
                HasFinished = false
            });
            nextCat = -1;
            Console.WriteLine(playerName + " was added");
            Console.WriteLine("They are player number " + this.HowManyPlayers());
            return true;
        }

        public int HowManyPlayers()
        {
            return players.Count;
        }

        public bool CanStartPlaying()
        {
            return this.HowManyPlayers() >= 2 && this.HowManyPlayers() <= 6;
        }

        public bool CanGetOutOfPenaltyBox()
        {
            if (currentPlayer.TimeinPrison == 1)
            {
                return true;
            }
            var rand = new Random();
            Console.WriteLine(currentPlayer.Name + " a une chance de sortir de 1/" + currentPlayer.TimeinPrison);
            var a = rand.Next(currentPlayer.TimeinPrison);
            if(a == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void InitGame()
        {
            if (currentPlayer == null) currentPlayer = players[0];
        }
        public EAction Roll(int roll)
        {
            Console.WriteLine("\n");
            Console.WriteLine("----------- Player " + (currentPlayer.Rank + 1) + " -----------");

            /*if (QuitTheGame())
            {
                players.Remove(currentPlayer);

                if (players.Count < 2)
                    return EAction.QUIT;
                return EAction.DELETE;
            }*/

            Console.WriteLine(currentPlayer.Name + " is the current player");
            bool canGetOut = CanGetOutOfPenaltyBox();
            if(canGetOut) Console.WriteLine("They have rolled a " + roll);

            if (currentPlayer.HasPenality)
            {
                if (canGetOut)
                {
                    _isGettingOutOfPenaltyBox = true;

                    Console.WriteLine(currentPlayer.Name + " is getting out of the penalty box");
                    currentPlayer.HasPenality = false;
                    currentPlayer.Place =+ roll;
                    if (currentPlayer.Place > 12) currentPlayer.Place =- 13;

                    Console.WriteLine(currentPlayer.Name + "'s new location is " + currentPlayer.Place);
                    Console.WriteLine("The category is " + CurrentCategory(currentPlayer.Place));
                    AskQuestion();
                }
                else
                {
                    Console.WriteLine(currentPlayer.Name + " is not getting out of the penalty box");
                    _isGettingOutOfPenaltyBox = false;
                }
            }
            else
            {
                currentPlayer.Place =+ roll;
                if (currentPlayer.Place > 12) currentPlayer.Place =- 13;

                Console.WriteLine(currentPlayer.Name + "'s new location is " + currentPlayer.Place);
                Console.WriteLine("The category is " + CurrentCategory(currentPlayer.Place));
                AskQuestion();
            }
            return EAction.KEEP;
        }

        private void AskQuestion()
        {

            if (currentPlayer.HasJoker == EJoker.HAS)
            {
                var rand = new Random();
                Console.WriteLine("Would you whish to use your joker ?");
                if (rand.Next(2) == 1)
                {
                    Console.WriteLine("I use my joker !");
                    currentPlayer.HasJoker = EJoker.PENDING;
                }
            }

            if (currentPlayer.HasJoker != EJoker.PENDING)
            {
                int opt = currentPlayer.Place;
                if (nextCat != -1)
                {
                    opt = nextCat;
                    nextCat = -1;
                }

                if (CurrentCategory(opt) == "Pop")
                {
                    Console.WriteLine(_popQuestions.First());
                    _popQuestions.RemoveFirst();
                    _popQuestions.AddLast("Pop Question " + _popQuestions.Count + 1);
                }
                if (CurrentCategory(opt) == "Science")
                {
                    Console.WriteLine(_scienceQuestions.First());
                    _scienceQuestions.RemoveFirst();
                    _scienceQuestions.AddLast("Pop Question " + _scienceQuestions.Count + 1);
                }
                if (CurrentCategory(opt) == "Sports")
                {
                    Console.WriteLine(_sportsQuestions.First());
                    _sportsQuestions.RemoveFirst();
                    _sportsQuestions.AddLast("Pop Question " + _sportsQuestions.Count + 1);
                }
                if (CurrentCategory(opt) == "Rock")
                {
                    Console.WriteLine(_rockQuestions.First());
                    _rockQuestions.RemoveFirst();
                    _rockQuestions.AddLast("Pop Question " + _rockQuestions.Count + 1);
                }
                if (CurrentCategory(opt) == "Techno")
                {
                    Console.WriteLine(_rockQuestions.First());
                    _rockQuestions.RemoveFirst();
                    _rockQuestions.AddLast("Pop Question " + _rockQuestions.Count + 1);
                }
            }
        }

        private string CurrentCategory(int Place)
        {
            if (Place == 0) return "Pop";
            if (Place == 4) return "Pop";
            if (Place == 8) return "Pop";
            if (Place == 1) return "Science";
            if (Place == 5) return "Science";
            if (Place == 9) return "Science";
            if (Place == 2) return "Sports";
            if (Place == 6) return "Sports";
            if (Place == 10) return "Sports";
            return isTechno ? "Techno" : "Rock";
        }

        public void ChangePlayer()
        {
            if (currentPlayer.Rank >= players.Count-1) currentPlayer = players[0];
            else currentPlayer = players.Where(x => x.Rank == currentPlayer.Rank+1).FirstOrDefault();
        }

        public bool WasCorrectlyAnswered()
        {
            if (currentPlayer.HasPenality)
            {
                if (_isGettingOutOfPenaltyBox)
                {
                    currentPlayer.ScoringLevel = 1;
                    Console.WriteLine("Answer was correct!!!!");
                    currentPlayer.Purses += 1; //Ajout d'un point
                    Console.WriteLine(currentPlayer.Name + " now has " + currentPlayer.Purses + " Gold Coins.");

                    var winner = DidPlayerWin();
                    ChangePlayer();

                    return winner;
                }
                else
                {
                    ChangePlayer();
                    return true;
                }
            }
            else
            {
                if(currentPlayer.HasJoker != EJoker.PENDING)
                    Console.WriteLine("Answer was corrent!!!!");
                currentPlayer.ScoringLevel += 1;
                currentPlayer.Purses += currentPlayer.HasJoker == EJoker.PENDING ? 0 : currentPlayer.ScoringLevel; //Ajoute un point si un utilisateur à toujours sont joker

                if (currentPlayer.HasJoker == EJoker.PENDING) currentPlayer.HasJoker = EJoker.CONSUMED;

                Console.WriteLine(currentPlayer.Name + " now has " + currentPlayer.Purses + " Gold Coins.");

                var winner = DidPlayerWin();
                ChangePlayer();

                return winner;
            }
        }

        public bool WrongAnswer()
        {
            Console.WriteLine("Question was incorrectly answered");
            Console.WriteLine(currentPlayer.Name + " was sent to the penalty box");
            currentPlayer.HasPenality = true;
            currentPlayer.ScoringLevel = 1;
            currentPlayer.TimeinPrison += 1;

            Console.WriteLine("Choose the cathegory of next player");
            Console.WriteLine("type a number between 0/10");

            var rand = new Random();
            int cat = rand.Next(10);
            Console.WriteLine("you choosed " + CurrentCategory(cat));
            nextCat = cat;
            ChangePlayer();
            return true;
        }

        public bool QuitTheGame()
        {
            var rand = new Random();
            if (rand.Next(8) == 1)
            {
                Console.WriteLine(currentPlayer.Name + " leaving the game...");
                return true;
            }
            return false;
        }

        private bool DidPlayerWin()
        {
            if(currentPlayer.Purses >= nbWinOr)
            {
                leaderBoard.Add(currentPlayer);
                currentPlayer.HasFinished = true;
            }
            return leaderBoard.Count != 3;
        }

    }

}
