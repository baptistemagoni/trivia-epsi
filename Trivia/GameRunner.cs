﻿using System;

namespace Trivia
{
    public class GameRunner
    {
        private static bool _notAWinner;

        public static void Main(string[] args)
        {
            Console.WriteLine("Choisissez le type de question musicale: 0: ROCK 1: TECHNO");
            int.TryParse(Console.ReadLine(), out int number);

            if (number == 1 || number == 0)
            {
                int ors = 0;
                do
                {
                    Console.WriteLine("Choissisez le nombre d'or pour gagner. Nb: 6 est le minimum");
                    int.TryParse(Console.ReadLine(), out ors);
                } while (ors < 6);

                var aGame = new Game
                {
                    isTechno = number == 1,
                    nbWinOr = ors
                };

                aGame.Add("Chet"); // Add players
                aGame.Add("Chet1"); // Add players
                aGame.Add("Chet2"); // Add players
                aGame.Add("Chet3"); // Add players
                aGame.Add("Chet4"); // Add players

                var rand = new Random();

                bool canStart = aGame.CanStartPlaying();
                if (canStart)
                {
                    aGame.InitGame();
                    do
                    {
                        if (!aGame.currentPlayer.HasFinished)
                        {
                            EAction action = aGame.Roll(rand.Next(5) + 1);

                            if (action == EAction.QUIT) break;

                            if (action != EAction.DELETE)
                            {
                                if (rand.Next(9) == 7)
                                {
                                    _notAWinner = aGame.WrongAnswer();
                                }
                                else
                                {
                                    _notAWinner = aGame.WasCorrectlyAnswered();
                                }
                            }
                            else
                            {
                                aGame.ChangePlayer();
                            }
                        }
                        else
                        {
                            aGame.ChangePlayer();
                        }

                        if(!_notAWinner)
                        {
                            for(int i = 0; i < aGame.leaderBoard.Count; i++)
                            {
                                Console.WriteLine(aGame.leaderBoard[i].Name + " : " + (i+1) + ". position - " + aGame.leaderBoard[i].Purses + " goldens");
                            }
                        }

                    } while (_notAWinner && canStart);
                }
                if (!canStart)
                {
                    Console.WriteLine("This party is over !");
                }
                else
                {
                    Console.WriteLine("ROCK: 0 TECHNO: 1");
                }
            }
        }
    }
}