﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trivia
{
    public class Player
    {

        public string Name { get; set; }

        public int Purses { get; set; }
        
        public bool HasPenality { get; set; }

        public int Place { get; set; }

        public int Rank { get; set; }

        public EJoker HasJoker { get; set; }

        public int ScoringLevel { get; set; }

        public int TimeinPrison { get; set; }

        public bool HasFinished { get; set; }

    }
}
