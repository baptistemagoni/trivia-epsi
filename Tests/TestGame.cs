using Trivia;
using Xunit;

namespace Tests
{
    public class TestGame
    {
        [Fact]
        public void CreateRockQuestion_ReturnString()
        {
            var game = new Game();
            game.CreateRockQuestion(1);

            Assert.Equal("Rock Question 1", game.CreateRockQuestion(1));
        }

        [Fact]
        public void HowManyPlayers_Return2Players()
        {
            var game = new Game();
            game.Add("Test 1");
            game.Add("Test 2");

            Assert.Equal(2, game.HowManyPlayers());
        }

        [Fact]
        public void CanStartPlaying_1PlayerReturnFalse()
        {
            var game = new Game();
            game.Add("Test 1");

            Assert.False(game.CanStartPlaying(), "A game can't be played if it has only 1 player");
        }

        [Fact]
        public void CanStartPlaying_2PlayersReturnTrue()
        {
            var game = new Game();
            game.Add("Test 1");
            game.Add("Test 2");

            Assert.True(game.CanStartPlaying(), "A game can be played if it has 2 players");
        }
    }
}